<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index()
    {
        $this->load->helper(array('form', 'url'));
        //$this->load->library('form_validation');
        //$this->load->model('modelCustomer');

        $this->load->view('dashboard');
    }

}
