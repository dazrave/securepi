<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>[SecurePi] Dashboard</title>

    <!-- Bootstrap -->
    <link href="<?= base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url("assets/css/custom.css"); ?>" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url("favicon.ico"); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <img src="<?= base_url("assets/images/RaspberryPiLogo.png"); ?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                hello world
            </div>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url("assets/js/bootstrap.js"); ?>"></script>
</body>
</html>
