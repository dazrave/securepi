<?php

class modelCustomer extends CI_Model {

    function checkCustomerEmail()
    {

        $this->db->select('cusCustomerID');
        $this->db->where('cusCustomerEmail', $_POST['email']);
        $query = $this->db->get('tblCustomer');

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    function addCustomer()
    {
        $this->cusCustomerFirstName   = $_POST['firstName'];
        $this->cusCustomerSecondName = $_POST['secondName'];
        $this->cusCustomerEmail = $_POST['email'];
        $this->cusCustomerMobile = $_POST['mobile'];
        $this->cusCustomerCreatedDate    = time();
        $this->cusCustomerLastVisited    = 0;
        $this->cusCustomerTimesVisted    = 0;
        $this->cusCustomerRating    = 0;
        $this->cusCustomerStatus    = 'new';

        $this->db->insert('tblCustomer', $this);

    }

    function updateCustomer($customerID)
    {
        // Get current data
        $query = $this->db->get_where('tblCustomer', array('cusCustomerID' => $customerID), 1, 0);
        $data = $query->result();

        // Check for change
        $insert->cusCustomerFirstName     = $_POST['firstName'] != $data[0]->cusCustomerFirstName ? $data[0]->cusCustomerFirstName : $_POST['firstName'];
        $insert->cusCustomerSecondName    = $_POST['secondName'] != $data[0]->cusCustomerSecondName ? $data[0]->cusCustomerSecondName : $_POST['secondName'];
        $insert->cusCustomerEmail         = $_POST['email'] != $data[0]->cusCustomerEmail ? $data[0]->cusCustomerEmail : $_POST['email'];
        $insert->cusCustomerMobile        = $_POST['mobile'] != $data[0]->cusCustomerMobile ? $data[0]->cusCustomerMobile : $_POST['mobile'];
        $insert->cusCustomerStatus        = $_POST['status'] != $data[0]->cusCustomerStatus ? $data[0]->cusCustomerStatus : $_POST['status'];

        // Update Customer
        $this->db->where('cusCustomerID', $customerID);
        $this->db->update('tblCustomer', $insert);

        // print_r ($insert);
    }

}

?>
