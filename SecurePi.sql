/* v1.0 */
DROP TABLE IF EXISTS `tblConfig`;
CREATE TABLE `tblConfig` (
  `conConfigID` INT(11) NOT NULL AUTO_INCREMENT,
  `conConfigSystemName` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`conConfigID`)
);

DROP TABLE IF EXISTS `tblUser`;
CREATE TABLE `tblUser` (
  `useUserID` INT(11) NOT NULL AUTO_INCREMENT,
  `useUserName` VARCHAR(255) NULL DEFAULT NULL,
  `useUserEmail` VARCHAR(255) NULL DEFAULT NULL,
  `useUserMobile` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`useUserID`)
);

DROP TABLE IF EXISTS `tblDevice`;
CREATE TABLE `tblDevice` (
  `priDeviceID` INT(11) NOT NULL AUTO_INCREMENT,
  `priDeviceName` VARCHAR(255) NULL DEFAULT NULL,
  `priDeviceStatus` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`priDeviceID`)
);

DROP TABLE IF EXISTS `linkUserDevive`;
CREATE TABLE `linkUserDevice` (
  `linkUserDeviceID` INT(11) NOT NULL AUTO_INCREMENT,
  `linkUserID` VARCHAR(255) NULL DEFAULT NULL,
  `linkDeviceID` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`linkUserDeviceID`)
);

/* Default Setup Details */
INSERT INTO tblConfig (conConfigSystemName) VALUES
('SecurePI');

INSERT INTO tblDevice (priDeviceName, priDeviceStatus) VALUES
('WOL', 0),
('LIGHTS', 0),
('SOUND', 0),
('DOOR', 0);

INSERT INTO tblUser (useUserName, useUserEmail, useUserMobile) VALUES
('Darren', 'darren@daphi.co.uk', '07949044923');

INSERT INTO linkUserDevice (linkUserID, linkDeviceID) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4);
